import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Saham',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;

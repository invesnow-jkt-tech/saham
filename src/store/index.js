import { createStore } from 'vuex';

const store = createStore({
  state() {
    return {
      memories: [
        {
          id: 'm1',
          image:
            'https://i.pinimg.com/564x/8b/d3/a2/8bd3a28d7e6583f62c7556f48e02da72.jpg',
          title: 'A trip into the mountains',
          description: 'It wasnt necessary, I WANT GO TO BED NOW!',
        },
        {
          id: 'm2',
          image:
            'https://i.pinimg.com/564x/24/61/32/2461321400ccdffe975dbfee0fdcb5d9.jpg',
          title: 'Surfing the sea side',
          description: 'It wasnt necessary, I WANT DRINK SOMETHING NOW!',
        },
        {
          id: 'm3',
          image:
            'https://i.pinimg.com/564x/c1/df/07/c1df072663877a3a514e99346984031a.jpg',
          title: 'Good eating',
          description: 'It wasnt necessary, I WANT BUKAPUASA NOW!',
        },
      ],
    };
  },
  mutations: {
    addMemory(state, memoryData) {
      const newMemory = {
        id: new Date().toISOString(),
        title: memoryData.title,
        image: memoryData.imageUrl,
        description: memoryData.description
      };

      state.memories.unshift(newMemory);
    }
  },
  actions: {
    addMemory(context, memoryData) {
      context.commit('addMemory', memoryData);
    }
  },
  getters: {
    memories(state) {
      return state.memories;
    },
    memory(state) {
      return (memoryId) => {
        return state.memories.find((memory) => memory.id === memoryId);
      };
    },
  },
});

export default store;